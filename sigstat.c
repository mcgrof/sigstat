/* Linux sigstat
 *
 * (C) 2020 Luis Chamberlain <mcgrof@kernel.org>
 *
 * Scrapes Linux /proc for signal information and makes sense of it all.
 */

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <stdbool.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <dirent.h>
#include <ctype.h>
#include <pwd.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/utsname.h>

/*
 * The actual list of unsupported signals varies by operating system. This
 * program is Linux specific as it processes /proc/ for signal information and
 * there is no generic way to extract each process signal information for each
 * OS. This program also relies on Linux glibc defines to figure out which
 * signals are reserved for use by libc and then which ones are real time
 * specific.
 */

#define GLIBC_VERSION (__GLIBC__ * 100 + __GLIBC_MINOR__)

#if GLIBC_VERSION <= 231
/* libc doesn't document this directly, but those who look shall find :) */
extern const char * const sys_sigabbrev[];
#define SIGABBREV(x) sys_sigabbrev[x]
#else
#define SIGABBREV(x) sigabbrev_np(x)
#endif

/*
 * As per glibc:
 *
 * A system that defines real time signals overrides __SIGRTMAX to be something
 * other than __SIGRTMIN. This also means we can count on __SIGRTMIN being the
 * first real time signal, meaning what Linux programs it for your architecture
 * in the kernel. SIGRTMIN then will be the application specific first real
 * time signal, that is, on top of libc. The values in between
 *
 * 	__SIGRTMIN .. SIGRTMIN
 *
 * are used by * libc, typically for helping threading implementation.
 */
static const char *sigstat_strsignal_abbrev(int sig, char *abbrev, size_t len)
{
	memset(abbrev, '\0', len);

	if (sig == 0 || sig >= NSIG) {
		snprintf(abbrev, len, "BOGUS_%02d", sig - _NSIG);
		return abbrev;
	}

	/* 
	 * The standard lower signals we can count on this being the kernel
	 * specific SIGRTMIN.
	 */
	if (sig < __SIGRTMIN) {
		memcpy(abbrev, SIGABBREV(sig), len);
		return abbrev;
	}

	/* This means your system should *not* have realtime signals */
	if (__SIGRTMAX == __SIGRTMIN) {
		snprintf(abbrev, len, "INVALID_%02d", sig);
		return abbrev;
	}

	/* 
	 * If we're dealing with a libc real time signal start counting
	 * after libc's version of SIGRTMIN
	 */
	if (sig >= SIGRTMIN) {
		if (sig == SIGRTMIN)
			snprintf(abbrev, len, "RTMIN");
		else if (sig == SIGRTMAX)
			snprintf(abbrev, len, "RTMAX");
		else
			snprintf(abbrev, len, "RTMIN+%02d", sig - SIGRTMIN);
	} else
		snprintf(abbrev, len, "LIBC+%02d", sig - __SIGRTMIN);

	return abbrev;
}

static bool is_number(char *name)
{
	char *c = name;

	while (*c != '\0') {
		if (!isdigit(*c))
			return false;
		c++;
	}

	return true;
}

/*
 * For instance SIGTERM is 15, but its actual mask value is
 * 1 << (15-1) = 0x4000
 */
static uint64_t mask_sig_val_num(int signum)
{
	return ((uint64_t) 1 << (signum -1));
}

static const char *name_signals(long long mask_in, char *signames, size_t len)
{
	unsigned int i;
	char abbrev[PATH_MAX];
	unsigned int n = 0, count = 0;
	char *c = signames;
	uint64_t mask = mask_in;
	uint64_t test_val = 0;

	for (i=1; i < NSIG; i++) {
		test_val = mask_sig_val_num(i);
		if (test_val & mask) {
			n = snprintf(c, len, "%s ",
				     sigstat_strsignal_abbrev(i, abbrev,
					   		      PATH_MAX));
			len -= n;
			c+=n;
			count++;
		}
	}

	if (count > 0)
		n = snprintf(c, len, ": %d signals", count);
	else
		n = snprintf(c, len, ": No signals");
	return signames;
}

int process_proc_signals(FILE *f, char *cmd, struct dirent *dirent)
{
	char *p;
	char *data;
	uint64_t queued_signals, resource_limit,
		 pend_sig_mask, shd_sig_mask,
		 block_sig_mask, ign_sig_mask,
		 cnt_sig_mask, signames_len = NSIG;
	char line[PATH_MAX];
	char sigq[PATH_MAX];
	int num_scanned;
	bool print_count = true;
	char *signames;

	signames = malloc(sizeof(char) * signames_len);
	if (!signames)
		return -ENOMEM;

	while (1) {
		memset(line, 0, sizeof(char) * PATH_MAX);
		memset(sigq, 0, sizeof(char) * PATH_MAX);
		memset(signames, '\0', sizeof(char) * signames_len);

		queued_signals = 0;
		resource_limit = 0;
		pend_sig_mask = 0;
		shd_sig_mask = 0;
		block_sig_mask = 0;
		ign_sig_mask = 0;
		cnt_sig_mask = 0;

		data = fgets(line, PATH_MAX, f);
		if (!data) {
			free(signames);
			return 0;
		}

		p = strtok(line, ":");
		if (!p) {
			free(signames);
			return -1;
		}
		if (print_count && strncmp(p, "SigQ", PATH_MAX) == 0) {
			p = strtok(NULL, ":");
			num_scanned = sscanf(p, "%" PRIu64 "/%"PRIu64,
					     &queued_signals, &resource_limit);
			if (num_scanned != 2) {
				fprintf(stderr, "Could not parse SigQ for %s\n",
					dirent->d_name);
				continue;
			}
			if (queued_signals != 0) {
				printf("SigQ (number of queued signals / resource limit) for pid %s (%s): %" PRIu64 "/%" PRIu64 "\n",
				       dirent->d_name, cmd, queued_signals, resource_limit);
			}
			continue;
		} else if (strncmp(p, "SigPnd", PATH_MAX) == 0) {
			p = strtok(NULL, ":");

			num_scanned = sscanf(p, "%" PRIx64, &pend_sig_mask);
			if (num_scanned != 1) {
				fprintf(stderr, "Could not parse SigPnd mask for pid %s\n",
					dirent->d_name);
				free(signames);
				return -1;
			}

			if (pend_sig_mask != 0)
				printf("SigPnd (per thread pending signals) found for pid %s (%s): 0x%016" PRIx64"\t: %s\n",
				       dirent->d_name, cmd, pend_sig_mask,
				       name_signals(pend_sig_mask, signames,
						    signames_len));
			continue;
		} else if (strncmp(p, "ShdPnd", PATH_MAX) == 0) {
			p = strtok(NULL, ":");

			num_scanned = sscanf(p, "%" PRIx64, &shd_sig_mask);
			if (num_scanned != 1) {
				fprintf(stderr, "Could not parse ShdPnd mask for pid %s\n",
					dirent->d_name);
				free(signames);
				return -1;
			}
			if (shd_sig_mask != 0)
				printf("SigPnd (process wide pending signals) found for pid (%s) %s: 0x%" PRIx64 "\t: %s\n",
				       dirent->d_name, cmd, shd_sig_mask,
				       name_signals(shd_sig_mask, signames,
						    signames_len));
			continue;
		} else if (strncmp(p, "SigBlk", PATH_MAX) == 0) {
			p = strtok(NULL, ":");

			num_scanned = sscanf(p, "%" PRIx64, &block_sig_mask);

			if (num_scanned != 1) {
				fprintf(stderr, "Could not parse SigBlk mask for pid %s\n",
					dirent->d_name);
				free(signames);
				return -1;
			}
			if (block_sig_mask != 0)
				printf("SigBlk (blocked signals) found for pid %s (%s): 0x%" PRIx64" \t: %s\n",
				       dirent->d_name, cmd, block_sig_mask,
				       name_signals(block_sig_mask, signames,
						    signames_len));
			continue;
		} else if (strncmp(p, "SigIgn", PATH_MAX) == 0) {
			p = strtok(NULL, ":");

			num_scanned = sscanf(p, "%" PRIx64, &ign_sig_mask);

			if (num_scanned != 1) {
				fprintf(stderr, "Could not parse SigIgn mask for pid %s\n",
					dirent->d_name);
				free(signames);
				return -1;
			}
			if (block_sig_mask != 0)
				printf("SigIgn (ignored signals) found for pid %s (%s): 0x%" PRIx64"\t: %s\n",
				       dirent->d_name, cmd, ign_sig_mask,
				       name_signals(ign_sig_mask, signames,
						    signames_len));
			continue;
		} else if (print_count && strncmp(p, "SigCgt", PATH_MAX) == 0) {
			p = strtok(NULL, ":");

			num_scanned = sscanf(p, "%" PRIx64, &cnt_sig_mask);
			if (num_scanned != 1) {
				fprintf(stderr, "Could not parse SigCgt mask for pid %s\n",
					dirent->d_name);
				free(signames);
				return -1;
			}
			if (cnt_sig_mask != 0)
				printf("SigCgt (signals caught) found for pid %s (%s): 0x%" PRIx64 "\t: %s\n",
				       dirent->d_name, cmd, cnt_sig_mask,
				       name_signals(cnt_sig_mask, signames,
						    signames_len));
			continue;
		}
	}
	free(signames);
}

static void str_chomp(char *str, char *dest_str, size_t size)
{
	unsigned int i, count = 0, max_str;
	char line[PATH_MAX];

	if (strlen(str) > PATH_MAX)
		return;

	memset(line, '\0', PATH_MAX);

	max_str = strlen(str) < PATH_MAX ? strlen(str) : size;

	for (i=0; i< max_str; i++) {
		if (!isspace(str[i]) && !isspace(str[i]) && isalnum(str[i])) {
			line[count] = str[i];
			count++;
		}
	}

	memcpy(dest_str, line, PATH_MAX);
}

static int read_proccess_signals(struct dirent *dirent)
{
	FILE *f;
	char path[PATH_MAX];
	char comm[PATH_MAX];
	char cmd[PATH_MAX];
	char *p;
	int r;

	/*
	 * We could also just open up /proc/%s/comm for the cmd and use that
	 * would mean opening two files instead of just one.
	 */
	snprintf(path, PATH_MAX, "/proc/%s/status", dirent->d_name);

	f = fopen(path, "r");
	if (f == NULL) {
		if (errno == EACCES ||
		    errno == ENOENT)
			return 0;
		else
			return errno;
	}

	p = fgets(comm, PATH_MAX, f);
	if (!p) {
		fclose(f);
		return 0;
	}

	p = strtok(comm, ":");
	if (!p) {
		fclose(f);
		return 0;
	}
	p = strtok(NULL, ":");
	if (!p) {
		fclose(f);
		return 0;
	}

	memset(cmd, '\0', PATH_MAX);
	str_chomp(p, cmd, PATH_MAX);

	r = process_proc_signals(f, cmd, dirent);

	fclose(f);

	return r;
}

static int run_cmd_check_signals(void)
{
	DIR *dir;
	struct dirent *dirent;
	int r;

	dir = opendir("/proc");
	if (!dir) {
		perror("Could not open /proc");
		return errno;
	}

	while (1) {
		errno = 0;
		dirent = readdir(dir);
		if (!dirent && errno) {
			perror("Could not get next directory entry\n");
			return errno;
		}

		/* We're done processing the directory */
		if (!dirent)
			break;

		if (dirent->d_type != DT_DIR)
			continue;

		if (!is_number(dirent->d_name))
			continue;

		r = read_proccess_signals(dirent);
		if (r != 0) {
			fprintf(stderr, "Could not read process %s\n",
				dirent->d_name);
			closedir(dir);
			return errno;
		}
	}

	closedir(dir);

	return 0;
}

static void usage(const char *name)
{
	printf("Usage: %s < -h | --help > | <optional-timeval-in-seconds>\n\n",
	       name);
	printf("Scrapes /proc/ for signal status for each process you have\n");
	printf("read access to and interprets results.\n");

	exit(0);
}

int main(int argc, char *argv[])
{
	long timeval = 1;
	int r;

	if (argc <= 1) {
		exit(run_cmd_check_signals());
	}

	if (argc == 2) {
		if (strncmp(argv[1], "-h", PATH_MAX) == 0 ||
		    strncmp(argv[1], "--help", PATH_MAX) == 0)
			usage(argv[0]);
		else {
			timeval = atol(argv[1]);
			if (!timeval) {
				fprintf(stderr,
					"Invalid input for time element: %s\n",
					argv[1]);
				usage(argv[0]);
			}
		}
	} else
		usage(argv[0]);

	while (true) {
		r = run_cmd_check_signals();
		if (r)
			exit(r);
		sleep(timeval);
	}

	exit(0);
}
