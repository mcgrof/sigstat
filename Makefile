all: sigstat

sigstat: sigstat.c
	gcc -Wall -D_GNU_SOURCE -o sigstat $?

clean:
	rm -f *.o sigstat
