Linux sigstat
=============

I was surprised there are no real tools yet to scape and make sense out of
Linux /proc signals, and deciphering them manually is just silly. So here's
a tool which tries to make sense of all that.

One of the challenges with signals is where exactly do your custom signals
start, and this will depend on what threading solution you use. This program
relies on Linux glibc defines to figure out which signals are reserved for use
by libc and then which ones are real time specific. To name signals it also
makes use of the undocumented libc sys_sigabbrev[].

Where should this live?
=======================

I've determined this should live in the procps project, but I'm creating
this standalone git tree to track other progress / changes in case that
ends up not being the right place for this.

You can track the merge request here:

https://gitlab.com/procps-ng/procps/-/merge_requests/98

SUSE Hackweek
=============

This itch was scratched during SUSE's 2020 Hackweek.
